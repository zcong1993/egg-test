module.exports = app => {
  class GithubService extends app.Service {
    async index(name) {
      const data = await this.ctx.curl('https://api.github.com/users/' + name, {
        dataType: 'json'
      })
      return data
    }
  }
  return GithubService
}
