module.exports = app => {
  class HomeController extends app.Controller {
    async index() {
      const data = {
        title: 'egg',
        content: 'Hello world!',
        login: 'zcong'
      }
      const name = this.ctx.query.name ? this.ctx.query.name : 'zcong1993'
      const userinfo = await this.ctx.service.github.index(name)
      this.ctx.body = Object.assign({}, data, userinfo.data)
      this.ctx.status = 200
    }
  }
  return HomeController
}
