describe('test/app/service/github.test.js', () => {
  it('should work well', async () => {
    const ctx = app.mockContext()
    const fixture = 'zcong1993'
    const res = await ctx.service.github.index(fixture)

    assert(res.data.login === fixture)
  })
})
