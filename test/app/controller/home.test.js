describe('test/app/controller/home.test.js', () => {
  it('should app auto init on setup.js', () => {
    // app is auto init at `test/.setup.js`
    assert(app)
    assert(mock)
    // mock alias
    assert(mm)
    assert(request)
  })

  it('should GET /', async () => {
    const res = await app.httpRequest()
      .get('/')
      .expect(200)
      .expect('Content-Type', /json/)

    assert(res.body.login === 'zcong1993')
  })
  it('should work well with user', async () => {
    const res = await app.httpRequest()
      .get('/?name=egoist')
      .expect(200)
      .expect('Content-Type', /json/)

    assert(res.body.login === 'egoist')
  })
})
